/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.formwidgets;

import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = "FormWidgets";
  private TextView log;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    log = findViewById(R.id.log);

    findViewById(R.id.icon).setOnClickListener(v -> log(R.string.icon_clicked));
    findViewById(R.id.button).setOnClickListener(
      v -> log(R.string.button_clicked));

    ((CompoundButton)findViewById(R.id.swytch))
      .setOnCheckedChangeListener((v, isChecked) ->
        log(isChecked ? R.string.switch_checked : R.string.switch_unchecked));

    ((CompoundButton)findViewById(R.id.checkbox))
      .setOnCheckedChangeListener((v, isChecked) ->
        log(
          isChecked ? R.string.checkbox_checked : R.string.checkbox_unchecked));

    ((RadioGroup)findViewById(R.id.radioGroup))
      .setOnCheckedChangeListener(this::onRadioGroupChange);

    ((SeekBar)findViewById(R.id.seekbar))
      .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
          String msg = getString(R.string.seekbar_changed, progress);

          log.setText(msg);
          Log.d(TAG, msg);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
          // ignored
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
          // ignored
        }
      });
  }

  private void onRadioGroupChange(RadioGroup group, int checkedId) {
    @StringRes int msg;

    switch (checkedId) {
      case R.id.radioButton1:
        msg = R.string.radiobutton1_checked;
        break;

      case R.id.radioButton2:
        msg = R.string.radiobutton2_checked;
        break;

      default:
        msg = R.string.radiobutton3_checked;
        break;
    }

    log(msg);
  }

  private void log(@StringRes int msg) {
    log.setText(msg);
    Log.d(TAG, getString(msg));
  }
}
